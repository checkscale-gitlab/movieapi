Movie-API
==========

Simple Docker + API example. Movie data from https://github.com/FEND16/movie-json-data/


Build
-----

Use::

    docker build -t movieapi .

This command builds a Docker image named `movieapi` using your current folder as 
the building context (the . at the end specifies the path of the building 
context to use).


Run
---

Specifying that the app needs to expose port 8000 locally (otherwise cannot connect),
and the server is **not** bound to localhost (the default)::

    docker run -p 8000:8000 movieapi uvicorn --host 0.0.0.0 main:app
    
Check in browser: http://0.0.0.0:8000/movie

Ctrl-C to stop.
    
    
Run Options
-----------

To run as a daemon process::

    docker run -d --name movieapi -p 8000:8000 movieapi uvicorn --host 0.0.0.0 main:app

Check::

    docker ps

Can::

    docker stop movieapi


Compose
-------

Use::

    docker-compose build
    docker-compose up
    
Check it is running::

    docker-compose ps
    
Then Ctrl-C to end.

To run as a daemon process::

    docker-compose up -d
    
To follow logs:

    docker-compose logs -f
    

More Ideas...
-------------

* Mount data outside of the container
* Use an `.env` file to dynamically configure e.g. port address
* Push image to DockerHub and use it to build a container


