#https://fastapi.tiangolo.com/tutorial/first-steps/
import json
from random import choice

from fastapi import FastAPI

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.get("/movie")
async def movie():
    with open("/movies.json", "r") as _f:
        movies = json.loads(_f.read())
    return {"movie": choice(movies)}

